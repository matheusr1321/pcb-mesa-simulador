# PCB mesa simulador

Para fazer o esquemático e board da PCB no Eagle, utilizou-se os seguintes tutoriais:

* [Vídeo 1: Getting Started with EAGLE Part 1 https://www.youtube.com/watch?v=j79RRCUsD2c&list=PL1rOC5j_Fyi6ZP1vBWyQFxpJ7d8T_BxoB&index=2 ]  
* [Vídeo 2: Getting Started with EAGLE Part 2 https://www.youtube.com/watch?v=SgT2aUhJQHA&list=PL1rOC5j_Fyi6ZP1vBWyQFxpJ7d8T_BxoB  ]  
* [Vídeo 3: How to Design PCB Layout using Eagle (CadSoft) https://www.youtube.com/watch?v=oM9dZ6gR2Fc  ]  
* [Vídeo 4: Naming and labeling net stubs in Cadsoft Eagle  https://www.youtube.com/watch?v=6ikZHzEYQcQ]
* [Vídeo 5: Change size of pads in Eagle Cad  https://www.youtube.com/watch?v=BBao0eHu3SQ ]  
* [ Fórum 1: How to increase connection size of pad to ground plane   https://forums.autodesk.com/t5/eagle-forum/help-please-increase-connection-size-of-pad-to-ground-plane/td-p/7555641      ]  


Observation: Last updated is the MARK3 in 09.04.21. That's the PCB Design version without circuit protection, Cubesat connection and current velocity EC motores reading. 

                                                                             
                                                                              
                                                                             
